<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Perfume
 * @package App
 */
class Perfume extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $incrementing = false;
}
