<?php


namespace App\Http\Controllers;

use App\Perfume;

/**
 * Class PerfumeController
 * @package App\Http\Controllers
 */
class PerfumeController extends Controller
{
    /**
     * @return mixed
     */
    public function list() {
        $perfume = Perfume::orderBy('name', 'asc')->paginate(15);
        return $perfume;
    }
}
