
# wangi-surga-backend

how to install

1. `composer install;`
2. edit .env file configure your database
3. `php artisan migrate;`
4. `php artisan serve`

it will run http://127.0.0.1:8000

API Routes
```
Register User
POST /api/register
# form-data parameter:
	email: (string)
	password: (string)
# return response
application/json example
{
    "access_token": "xxxx",
    "token_type": "bearer",
    "expires_in": 216000
}


Login
POST /api/login
# form-data parameter:
	email: (string)
	password: (string)
# return response example
application/json
{
    "access_token": "xxxx",
    "token_type": "bearer",
    "expires_in": 216000
}

List Perfume
GET /api/perfumes
# header data:
Authorization: Bearer xxxx[access_token]
Accept: application/json
# return response example
application/json
{
    "current_page": 1,
    "data": [
        {
            "id": "7211655b-02e0-4e9f-81ea-c8a2a7f76aaf",
            "name": "Abagail Adams",
            "description": "Voluptatum corrupti aut nulla deserunt ratione sunt maiores. Vero aliquid aut itaque perspiciatis nihil. Est sint voluptates accusamus soluta. Eligendi ut quis totam inventore ad nihil aut.",
            "quantity": 163,
            "weight": 900.13,
            "size": "203",
            "views": 1,
            "category": "woman",
            "price": 0,
            "photo": "https://lorempixel.com/640/480/?20208",
            "created": "2019-10-31 14:59:44",
            "modified": "2019-11-01 14:04:00"
        },
    ],
    "first_page_url": "http://127.0.0.1:8000/api/perfumes?page=1",
    "from": 1,
    "last_page": 44,
    "last_page_url": "http://127.0.0.1:8000/api/perfumes?page=44",
    "next_page_url": "http://127.0.0.1:8000/api/perfumes?page=2",
    "path": "http://127.0.0.1:8000/api/perfumes",
    "per_page": 15,
    "prev_page_url": null,
    "to": 15,
    "total": 650
}


```
