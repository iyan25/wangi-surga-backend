<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfumes', function (Blueprint $table) {
            $table->string('id', 36);
            $table->primary('id');

            $table->string('name');
            $table->text('description');
            $table->integer('quantity')->unsigned()->default(0);
            $table->double('weight');
            $table->string('size', 50);
            $table->integer('views')->unsigned()->default(0);
            $table->string('category', 100);
            $table->integer('price')->unsigned()->default(0);
            $table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfumes');
    }
}
